#include "drl.hpp"

using namespace DRL;

vector<Statement_t> Parser::parse()
{
	vector<Statement_t> statements;

	Tokenizer t(source);
	tokens = t.tokenize();

	cursor = 0;

	while(!end()) {
		statements.push_back(nextStatement());
	}

	return statements;
}

vector<Token> Parser::nextLine()
{
	vector<Token> tokens;

	while(!end()) {
		const auto& t = next();
		if(t.type == NEWLINE_T) break;

		tokens.push_back(t);
	}

	return tokens;
}

Statement_t Parser::nextStatement()
{
	const auto tokens = nextLine();
	if(tokens.empty()) return nullptr;

	unsigned int tabs = 0;

	while(tabs < tokens.size()) {
		if(tokens[tabs].type != SCOPE_TAB_T) break;
		++tabs;
	}

	if(tabs >= tokens.size()) return nullptr;

	const auto& begin = tokens[tabs];

	switch(begin.type) {
		case CHAR_T:
		case STRING_T:
		case NULL_T:
		case BOOL_T:
		case OPERATOR_T:
		case COMPARATOR_T:
		case NUMERIC_T: {
			// TODO Unexpected
			break;
		}

		case PUNCTUATION_T: {
			// TODO ?
			break;
		}

		case KEYWORD_T: {
			return handleKeyword(tokens, tabs);
		}

		case IDENTIFIER_T: {
			return handleIdentifier(tokens, tabs);
		}

		case UNKNOWN_T: {
			// TODO Syntax error
			break;
		}

		default: {
			return nullptr;
		}
	}
}

Statement_t Parser::handleKeyword(const vector<Token>& tokens,
	const unsigned int tabs)
{
	const auto& keyword = tokens[tabs];

	switch(keyword.value.front()) {
		case CONST_K: {
			// TODO
			break;
		}

		case LET_K: {
			// TODO
			break;
		}

		case IF_K: {
			// TODO
			break;
		}

		case ELSE_K: {
			// TODO
			break;
		}

		case WHILE_K: {
			// TODO
			break;
		}

		case BREAK_K: {
			// TODO
			break;
		}

		case SKIP_K: {
			// TODO
			break;
		}

		case FOR_K: {
			// TODO
			break;
		}

		case EXEC_K: {
			// TODO
			break;
		}

		case FUNC_K: {
			// TODO
			break;
		}

		case RET_K: {
			// TODO
			break;
		}
	}
}

Statement_t Parser::handleIdentifier(const vector<Token>& tokens,
	const unsigned int tabs)
{
	// TODO Variable definition or function call
}
