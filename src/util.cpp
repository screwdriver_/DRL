#include "drl.hpp"

using namespace DRL;

Type DRL::getType(const string& type)
{
	// TODO
}

string DRL::getType(const Type type)
{
	switch(type) {
		case I8: {
			return "i8";
		}

		case UI8: {
			return "ui8";
		}

		case I16: {
			return "i16";
		}

		case UI16: {
			return "ui16";
		}

		case I32: {
			return "i32";
		}

		case UI32: {
			return "ui32";
		}

		case I64: {
			return "i64";
		}

		case UI64: {
			return "ui64";
		}

		case FLO: {
			return "flo";
		}

		case DOU: {
			return "dou";
		}

		case BOOL: {
			return "bool";
		}

		case STR: {
			return "str";
		}

		case DOC: {
			return "doc";
		}

		case JSON: {
			return "json";
		}

		case ANY: {
			return "any";
		}
	}
}

string DRL::getCppType(const Type type)
{
	switch(type) {
		case I8: {
			return "int8_t";
		}

		case UI8: {
			return "uint8_t";
		}

		case I16: {
			return "int16_t";
		}

		case UI16: {
			return "uint16_t";
		}

		case I32: {
			return "int32_t";
		}

		case UI32: {
			return "uint32_t";
		}

		case I64: {
			return "int64_t";
		}

		case UI64: {
			return "uint64_t";
		}

		case FLO: {
			return "float";
		}

		case DOU: {
			return "double";
		}

		case BOOL: {
			return "bool";
		}

		case STR: {
			return "string";
		}

		case DOC: {
			return "Doc";
		}

		case JSON: {
			return "JSON::Element_t";
		}

		case ANY: {
			return "void*";
		}
	}
}
