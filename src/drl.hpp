#ifndef DRL_HPP
#define DRL_HPP

#include<exception>
#include<memory>
#include<string>
#include<unordered_map>
#include<vector>

#include<tungsten.hpp>

namespace DRL
{
	using namespace std;

	const char COMMENT_BEGIN = '#';

	const char CHAR = '\'',
		STRING = '\"',
		BACKSLASH = '\\';

	const char CONSTANT_USE = '$';

	const char ARRAY_OPEN = '[',
		ARRAY_CLOSE = ']',
		ARRAY_LOCK = '*';

	const char FOR_SEPARATOR = ';';

	const char BRACKET_OPEN = '(',
		BRACKET_CLOSE = ')',
		VOID = '.';

	const char TEMPLATE_ARG = '?',
		TEMPLATE_OPEN = '<',
		TEMPLATE_CLOSE = '>';

	const string ASSIGN_OP = "=",
		ADD_ASSIGN_OP = "+=",
		SUBTRACT_ASSIGN_OP = "-=",
		MULTIPLY_ASSIGN_OP = "*=",
		DIVIDE_ASSIGN_OP = "/=",
		MODULO_ASSIGN_OP = "%=";

	const string ADD_OP = "+",
		SUBTRACT_OP = "-",
		MULTIPLY_OP = "*",
		DIVIDE_OP = "/",
		MODULO_OP = "%";

	const string NOT_OP = "!";

	const string EQUAL = "==",
		NOT_EQUAL = "!=",
		LOWER = "<",
		LOWER_OR_EQUAL = "<=",
		HIGHER = ">",
		HIGHER_OR_EQUAL = ">=";

	const string AND = "&&",
		OR = "||";

	enum Operator : uint8_t
	{
		ASSIGN_O = 0,
		ADD_ASSIGN_O = 1,
		SUBTRACT_ASSIGN_O = 2,
		MULTIPLY_ASSIGN_O = 3,
		DIVIDE_ASSIGN_O = 4,
		MODULO_ASSIGN_O = 5,

		ADD_O = 6,
		SUBTRACT_O = 7,
		MULTIPLY_O = 8,
		DIVIDE_O = 9,
		MODULO_O = 10,

		NOT_O = 11
	};

	enum Comparator : uint8_t
	{
		EQUAL_C = 0,
		NOT_EQUAL_C = 1,
		LOWER_C = 2,
		LOWER_OR_EQUAL_C = 3,
		HIGHER_C = 4,
		HIGHER_OR_EQUAL_C = 5,

		AND_C = 6,
		OR_C = 7
	};

	enum Keyword : uint8_t
	{
		CONST_K = 0,
		LET_K = 1,
		IF_K = 2,
		ELSE_K = 3,
		WHILE_K = 4,
		BREAK_K = 5,
		SKIP_K = 6,
		FOR_K = 7,
		EXEC_K = 8,
		FUNC_K = 9,
		RET_K = 10
	};

	enum Type : uint8_t
	{
		I8 = 0,
		UI8 = 1,
		I16 = 2,
		UI16 = 3,
		I32 = 4,
		UI32 = 5,
		I64 = 6,
		UI64 = 7,
		FLO = 8,
		DOU = 9,
		BOOL = 10,
		STR = 11,
		DOC = 12,
		JSON = 13,
		ANY = 14
	};

	Type getType(const string& type);
	string getType(const Type type);

	string getCppType(const Type type);

	enum TokenType : uint8_t
	{
		NEWLINE_T = 0,
		SCOPE_TAB_T = 1,
		COMMENT_T = 2,
		CHAR_T = 3,
		STRING_T = 4,
		NULL_T = 5,
		BOOL_T = 6,
		PUNCTUATION_T = 7,
		OPERATOR_T = 8,
		COMPARATOR_T = 9,
		KEYWORD_T = 10,
		NUMERIC_T = 11,
		IDENTIFIER_T = 12,
		UNKNOWN_T = 13
	};

	struct Token
	{
		const TokenType type;
		const string value;

		inline Token(const TokenType&& type)
			: type{type}, value()
		{}

		inline Token(const TokenType&& type, const char value)
			: type{type}, value{string() + value}
		{}

		inline Token(const TokenType&& type, const string& value)
			: type{type}, value{value}
		{}
	};

	class syntax_error : public exception
	{
		public:
			inline syntax_error(const Token& token,
				const string& message)
				: token{token}, message{message}
			{}

			inline const Token& getToken() const
			{
				return token;
			}

			inline const string& getMessage() const
			{
				return message;
			}

			inline virtual const char* what() const noexcept
			{
				return message.c_str();
			}

		private:
			const Token token;
			const string message;
	};

	class Tokenizer
	{
		public:
			inline Tokenizer(const string& source)
				: source{source}
			{}

			vector<Token> tokenize();

		private:
			const string source;

			unsigned int cursor;

			inline char peek() const
			{
				return source[cursor];
			}

			inline char next()
			{
				return source[cursor++];
			}

			inline void skip()
			{
				++cursor;
			}

			inline bool end() const
			{
				return (cursor >= source.size());
			}

			inline void skipSpaces()
			{
				if(end()) return;
				while(peek() == ' ') skip();
			}

			Token nextToken();
			void skipToken();

			string nextComment();
			char nextChar();
			string nextString();

			bool check(const string& str) const;

			inline bool check(const string&& str) const
			{
				return check(str);
			}

			bool checkNull() const;
			bool checkBoolean(bool* value) const;

			bool isPunctuation(const char c);

			bool checkOperator(Operator* value) const;
			bool checkComparator(Comparator* value) const;
			bool checkKeyword(Keyword* value) const;

			string nextElement();
	};

	struct Variable
	{
		const Type type;
		const uint32_t size;
		const bool locked;

		inline Variable(const Type type,
			const uint32_t size,
			const bool locked)
			: type{type},
			size{size},
			locked{locked}
		{}
	};

	struct Function
	{
		const uint32_t begin_inst;

		const bool void_return;
		const Type return_type;

		const vector<Type> arguments;
	};

	struct ProgramData
	{
		unordered_map<string, string> constants;
		vector<Variable> variables;

		unordered_map<string, Function> functions;
		// TODO Templates
	};

	class Statement
	{
		public:
			virtual void toTungsten(Tungsten::Program& program,
				ProgramData& data) const = 0;
			virtual string toCpp() const = 0;

			virtual string toString() const = 0;
	};

	typedef shared_ptr<Statement> Statement_t;

	class Parser
	{
		public:
			inline Parser(const string& source)
				: source{source}
			{}

			vector<Statement_t> parse();

		private:
			const string source;

			vector<Token> tokens;

			unsigned int cursor;

			inline const Token& peek() const
			{
				return tokens[cursor];
			}

			inline const Token& next()
			{
				return tokens[cursor++];
			}

			inline void skip()
			{
				++cursor;
			}

			inline bool end() const
			{
				return (cursor >= tokens.size());
			}

			vector<Token> nextLine();

			Statement_t nextStatement();

			Statement_t handleKeyword(const vector<Token>& tokens,
				const unsigned int tabs);
			Statement_t handleIdentifier(
				const vector<Token>& tokens,
				const unsigned int tabs);
	};

	class Compiler
	{
		public:
			inline Compiler(const string& source)
				: source{source}
			{}

			void parse();
			void compile(const string& output);

			inline void compile(const string&& output)
			{
				compile(output);
			}

		private:
			const string source;
	};
}

#endif
