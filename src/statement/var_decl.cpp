#include "statement.hpp"

using namespace DRL;

VarDeclStatement::VarDeclStatement(const Type type, const string& name)
	: type{type}, name{name}
{
	// TODO Check name validity
}

VarDeclStatement::VarDeclStatement(const Type type, const string& name,
	const Statement_t& value)
	: type{type}, name{name}, value{value}
{
	// TODO Check name validity
}

void VarDeclStatement::toTungsten(Tungsten::Program& program,
	ProgramData& data) const
{
	data.variables.emplace_back(type, 1, true);

	uint8_t a[4];
	a[0] = 32;
	a[1] = data.variables.size() - 1;

	uint32_t args;
	encode(args, a);

	program.instructions.emplace_back(VCREATE, args);

	if(value != nullptr) {
		value->toTungsten(program, data);
	}
}

string VarDeclStatement::toCpp() const
{
	// TODO Putting const when possible
	string str = getCppType(type) + ' ' + name;

	if(value != nullptr) {
		str += " = " + value->toCpp();
	}

	str += ';';
	return str;
}

string VarDeclStatement::toString() const
{
	string str("Declaration of variable `");
	str += name + "` of type `" + getType(type) + '`';

	if(value != nullptr) {
		str += " with value `" + value->toString() + '`';
	}

	return str;
}
