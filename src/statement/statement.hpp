#ifndef STATEMENT_HPP
#define STATEMENT_HPP

#include "../drl.hpp"

namespace DRL
{
	using namespace std;

	class VarDeclStatement : public Statement
	{
		public:
			VarDeclStatement(const Type type, const string& name);
			VarDeclStatement(const Type type, const string& name,
				const Statement_t& value);

			void toTungsten(Tungsten::Program& program,
				ProgramData& data) const override;
			string toCpp() const override;

			string toString() const override;

		private:
			const Type type;
			const string name;

			const Statement_t value;
	};

	class ValueStatement : public Statement
	{
		public:
			ValueStatement(const Type type, const string& value);

			void toTungsten(Tungsten::Program& program,
				ProgramData& data) const override;
			string toCpp() const override;

			string toString() const override;

		private:
			const Type type;
			const Statement_t value;
	};
}

#endif
