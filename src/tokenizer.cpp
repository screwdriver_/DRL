#include "drl.hpp"

#include<algorithm>
#include<unordered_map>

using namespace DRL;

const vector<char> punctuations = {
	CONSTANT_USE,

	ARRAY_OPEN,
	ARRAY_CLOSE,
	ARRAY_LOCK,

	FOR_SEPARATOR,

	BRACKET_OPEN,
	BRACKET_CLOSE,
	VOID,

	TEMPLATE_ARG,
	TEMPLATE_OPEN,
	TEMPLATE_CLOSE
};

const unordered_map<string, Operator> operators = {
	{ASSIGN_OP, ASSIGN_O},
	{ADD_ASSIGN_OP, ADD_ASSIGN_O},
	{SUBTRACT_ASSIGN_OP, SUBTRACT_ASSIGN_O},
	{MULTIPLY_ASSIGN_OP, MULTIPLY_ASSIGN_O},
	{DIVIDE_ASSIGN_OP, DIVIDE_ASSIGN_O},
	{MODULO_ASSIGN_OP, MODULO_ASSIGN_O},

	{ADD_OP, ADD_O},
	{SUBTRACT_OP, SUBTRACT_O},
	{MULTIPLY_OP, MULTIPLY_O},
	{DIVIDE_OP, DIVIDE_O},
	{MODULO_OP, MODULO_O},

	{NOT_OP, NOT_O}
};

const unordered_map<string, Comparator> comparators = {
	{EQUAL, EQUAL_C},
	{NOT_EQUAL, NOT_EQUAL_C},
	{LOWER, LOWER_C},
	{LOWER_OR_EQUAL, LOWER_OR_EQUAL_C},
	{HIGHER, HIGHER_C},
	{HIGHER_OR_EQUAL, HIGHER_OR_EQUAL_C},

	{AND, AND_C},
	{OR, OR_C}
};

const unordered_map<string, Keyword> keywords = {
	{"CONST", CONST_K},
	{"LET", LET_K},
	{"IF", IF_K},
	{"ELSE", ELSE_K},
	{"WHILE", WHILE_K},
	{"BREAK", BREAK_K},
	{"SKIP", SKIP_K},
	{"FOR", FOR_K},
	{"EXEC", EXEC_K},
	{"FUNC", FUNC_K},
	{"RET", RET_K}
};

vector<Token> Tokenizer::tokenize()
{
	cursor = 0;

	vector<Token> tokens;

	while(!end()) {
		tokens.push_back(nextToken());
	}

	return tokens;
}

Token Tokenizer::nextToken()
{
	skipSpaces();

	const auto begin = peek();

	switch(begin) {
		case '\n': {
			skip();
			return Token(NEWLINE_T);
		}

		case '\t': {
			skip();
			return Token(SCOPE_TAB_T);
		}

		case '#': {
			return Token(COMMENT_T, nextComment());
		}

		case '\'': {
			return Token(CHAR_T, nextChar());
		}

		case '"': {
			return Token(STRING_T, nextString());
		}
	}

	if(checkNull()) {
		skipToken();
		return Token(NULL_T);
	}

	bool bool_val;

	if(checkBoolean(&bool_val)) {
		skipToken();
		return Token(BOOL_T, (char) bool_val);
	}

	if(isPunctuation(begin)) {
		skip();
		return Token(PUNCTUATION_T, begin);
	}

	Operator op_val;

	if(checkOperator(&op_val)) {
		skipToken();
		return Token(OPERATOR_T, (char) op_val);
	}

	Comparator comp_val;

	if(checkComparator(&comp_val)) {
		skipToken();
		return Token(COMPARATOR_T, (char) comp_val);
	}

	Keyword key_val;

	if(checkKeyword(&key_val)) {
		skipToken();
		return Token(KEYWORD_T, (char) key_val);
	}

	if(isdigit(begin) || begin == '.') {
		return Token(NUMERIC_T, nextElement());
	} else {
		return Token(IDENTIFIER_T, nextElement());
	}

	return Token(UNKNOWN_T);
}

void Tokenizer::skipToken()
{
	while(!end() && peek() != ' ') {
		skip();
	}

	skipSpaces();
}

string Tokenizer::nextComment()
{
	string str;
	char c;

	while((c = peek()) != '\n') {
		skip();
		str += c;
	}

	return str;
}

char Tokenizer::nextChar()
{
	skip();

	bool backslash = false;

	if(peek() == '\\') {
		skip();
		backslash = true;
	}

	const char c = next();

	if(peek() != '\'') {
		throw invalid_argument("Expected `'` at end of character!");
	}

	skip();

	if(!backslash) return c;
	if(c == '"' || c == '\'') return c;

	// TODO Escape sequences
}

string Tokenizer::nextString()
{
	skip();

	string str;
	bool backslash = false;

	while(true) {
		const auto c = next();

		if(c == '"') {
			if(backslash) {
				backslash = false;
			} else {
				break;
			}
		} else if(c == '\\') {
			if(!backslash) {
				backslash = true;
				continue;
			} else {
				backslash = false;
			}
		} else {
			backslash = false;
		}

		str += c;
	}

	return str;
}

bool Tokenizer::check(const string& str) const
{
	for(unsigned int i = 0; i < str.size(); ++i) {
		if(cursor + i >= source.size()) return false;
		if(source[cursor + i] != str[i]) return false;
	}

	return true;
}

bool Tokenizer::checkNull() const
{
	return check("null");
}

bool Tokenizer::checkBoolean(bool* value) const
{
	if(check("true")) {
		*value = true;
		return true;
	} else if(check("false")) {
		*value = false;
		return true;
	}

	return false;
}

bool Tokenizer::isPunctuation(const char c)
{
	return any_of(punctuations.cbegin(), punctuations.cend(),
		[&c](const char c1)
		{
			return (c1 == c);
		});
}

bool Tokenizer::checkOperator(Operator* value) const
{
	for(const auto& o : operators) {
		if(check(o.first)) {
			*value = o.second;
			return true;
		}
	}

	return false;
}

bool Tokenizer::checkComparator(Comparator* value) const
{
	for(const auto& c : comparators) {
		if(check(c.first)) {
			*value = c.second;
			return true;
		}
	}

	return false;
}

bool Tokenizer::checkKeyword(Keyword* value) const
{
	for(const auto& k : keywords) {
		if(check(k.first)) {
			*value = k.second;
			return true;
		}
	}

	return false;
}

string Tokenizer::nextElement()
{
	string str;
	char c;

	while((c = peek()) != ' '
		|| c == '\t' || c == '\n') {
		skip();
		str += c;
	}

	return str;
}
