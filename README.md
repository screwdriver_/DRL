# DRL

Document Request Language is a language which allows to write requests to use a document database.

The language can be both interpreted or compiled, according to needs.



## Syntax

A request is a program which contains all the instructions to perform to execute the request.

Here are all the keywords reserved by the language:

| Name      | Description                                           |
|-----------|-------------------------------------------------------|
| ``CONST`` | Declares a constant                                   |
| ``LET``   | Declares a variable                                   |
| ``IF``    | Declares a condition block                            |
| ``ELSE``  | Declares an else block                                |
| ``WHILE`` | Declares a while loop                                 |
| ``BREAK`` | Ends the current loop                                 |
| ``SKIP``  | Skips the remaining instructions of the current scope |
| ``FOR``   | Declares a for loop                                   |
| ``EXEC``  | Invokes a internal or external function               |
| ``FUNC``  | Declares a function                                   |
| ``RET``   | Returns the function                                  |



Here are the all data types:

| Name     | Size (in bytes) | Description               |
|----------|-----------------|---------------------------|
| ``i8``   | 1               | An integer or a character |
| ``ui8``  | 1               | An unsigned integer       |
| ``i16``  | 2               | An integer                |
| ``ui16`` | 2               | An unsigned integer       |
| ``i32``  | 4               | An integer                |
| ``ui32`` | 4               | An unsigned integer       |
| ``i64``  | 8               | An integer                |
| ``ui64`` | 8               | An unsigned integer       |
| ``flo``  | 4               | A floating-point integer  |
| ``dou``  | 8               | A floating-point integer  |
| ``bool`` | 1               | A boolean                 |
| ``str``  | *dynamic*       | A character sequence      |
| ``doc``  | *dynamic*       | A document                |
| ``json`` | *dynamic*       | A json object             |
| ``any``  | 8               | Any type                  |

**Note**:
- The ``str`` type acts like an array of ``i8``
- Since the ``any`` type corresponds to a void pointer in C++, other operations than assignement are forbidden on it.



Here are the all operators:

| Name       | Operator | Description                                                        |
|------------|----------|--------------------------------------------------------------------|
| Assign     | ``=``    | Assigns a value to a variable                                      |
| Add assign | ``+=``   | Adds the value to the variable and assigns it                      |
| Sub assign | ``-=``   | Subtracts the value to the variable and assigns it                 |
| Mul assign | ``*=``   | Multiplies the value by the variable and assigns it                |
| Div assign | ``/=``   | Divides the variable by the value and assigns it                   |
| Mod assign | ``%=``   | Performs a modulo operation to the variable and assigns the result |
| Increment  | ``++``   | Increments the variable                                            |
| Decrement  | ``--``   | Decrements the variable                                            |
| Add        | ``+``    | Adds values                                                        |
| Subtract   | ``-``    | Subtracts values                                                   |
| Multiply   | ``*``    | Multiplies values                                                  |
| Divide     | ``/``    | Divides values                                                     |
| Modulo     | ``%``    | Performs a modulo on values                                        |
| Not        | ``!``    | Inverts a boolean value                                            |



Here are all the comparators:

| Comparator | Description                                                        |
|------------|--------------------------------------------------------------------|
| ``==``     | Checks if the two operands are equal                               |
| ``!=``     | Checks if the two operands are not equal                           |
| ``<``      | Checks if the first operand is lower than the second one           |
| ``<=``     | Checks if the first operand is lower than the second one or equal  |
| ``>``      | Checks if the first operand is higher than the second one          |
| ``>=``     | Checks if the first operand is higher than the second one or equal |

| Comparator | Description                                         |
|------------|-----------------------------------------------------|
| ``&&``     | True if the two operands are true, else false       |
| ``||``     | True if one of the two operands if true, else false |



### Comments

Comments are parts of the program that are ignored by the compiler.

A comment can be declared as follows:

```
# Here is a comment
```



### Scopes

Scopes are blocks that defines where variables can be accessed and/or whether a part of the code should be performed or not.

The main scope is the parts of the program where no other scope have been created.

A scope can be defined by just adding a tab:

```
# Main scope
	# Sub scope
```

The variables inside a scope are accessible only from the current scope and from sub scopes.
When the current scope comes to its end, all the variables declared in it are removed.



### Constants

Constants are variables that cannot be modified. A constant must be defined at declaration.

Here is an example of a constant:

```
CONST foo = "bar"
```

The previous declaration defines a constant named ``foo`` with the value ``"bar"``.
The constant can be accessed from everywhere in the code like this:

```
$foo
```

The previous sequence will be replaced by ``"bar"``.

Two constants cannot have the same name.



### Variables

Variables allow to pair data to a name and to access it later in the program.

A variable can be declared like this:

```
LET str foo = "bar"
```

The previous declaration defines a variables named ``foo`` of type ``str`` with the value ``"bar"``.

Two variables cannot have the same name but a constant and a variable can, since they are differencied by the ``$`` used to get the value of a constant.

### Arrays

Arrays allow to store several values inside one variable.

An array can be declared like this:

```
LET i8[] foo
```

Arrays grow dynamicly when needed, but a specified size can be reserved at initialization:

```
LET i8[4] foo
```

The above array has an initial size of 4 elements.

Arrays can also be locked, in this case, its size can't change anymore after initialization:

```
LET i8[4]* foo
```

The above array is locked with a size of 4 elements.

After being initialized, elements can be accessed or defined using their id:

```
foo[0] = 1
```

Finally, the size of an array can be retrieved by using it as a normal variable (thus, the type will be **ui32**).



### Conditions

Conditions allow to perform a scope only if the given condition is fullfilled.

Here is an example of condition:

```
IF 1 == 1
	# Code
```

The instructions after the IF statement will be performed only if the given condition is fullfilled.

Else statements can be added to perform instructions if the condition is not fullfilled.

Example:

```
IF 1 == 1
	# Code
ELSE
	# Code
```

Else-if statements can also be added to check multiple conditions:

```
IF 1 == 1
	# Code
ELSE IF 1 == 2
	# Code
ELSE
	# Code
```



### Loops

Loops are scopes executed repetively until the associated condition is no longer fullfilled.

#### While loops

While loops are simple conditional loops, they are declared like so:

```
WHILE 1 == 1
	# Code
```



#### For loops

For loops allow to iterate over a specified range.

For example:

```
FOR ui32 n = 0; n < 100; n++
	# Code
```

The above loop will be executed 100 times.



### Functions

Functions allow to associate a block of code with a name. Functions can take parameters and return a value.

Here is an example of function declaration:

```
FUNC . foo(i8 bar)
	# Code
```

The above declaration creates a function ``foo`` which takes one argument ``bar`` of type ``i8`` and returns no value.

In this function, the ``bar`` variable will be defined with the value passed in argument to the function. This variable will be usable only in this function.

If a function has several arguments, they have to be seperated by a ``,``.

Two functions can't have the same name unless they have different arguments.

A function can also return a value, here is an example:

```
FUNC i8 foo()
	RET 0
```

The above function returns the value ``0`` of type ``i8``.



#### Default functions

The following functions are available by default:
- **ui64 timestamp()**: Returns the current timestamp
- **double rand()**: Returns a random number between 0 and 1
- **str uuid()**: Returns a random version 4 UUID

- **doc get\_doc(str path)**: Returns the specified document (null if it doesn't exist)
- **doc[] get\_docs(str condition)**: Returns the documents that fit the specified condition
- **str[] list\_docs(str path, bool recursive)**: Returns the documents of the specified folder
- **doc put(str path)**: Creates a new document
- **doc put(str path, json content)**: Creates a new document
- **. rename(doc d, str new\_name)**: Renames the specified document
- **doc copy(doc d, str dest)**: Copies a document
- **. move(doc d, str dest)**: Moves a document
- **. set(doc d, json content, bool merge)**: Sets the content of a document
- **. remove(doc d, str field)**: Removes a field in a document
- **. delete(doc d)**: Deletes a document

- **any process(str name, bool blocking)**: Runs a subprocess
- **any process(str name, any[] parameters, bool blocking)**: Runs a subprocess
- **. error(str err)**: Kills the process and subprocesses and throws the specified error

- **?t sin(?t n)**: Computes the sine function of the given value
- **?t cos(?t n)**: Computes the cosine function of the given value
- **?t tan(?t n)**: Computes the tangent function of the given value
- **?t asin(?t n)**: Computes the arcsine function of the given value
- **?t acos(?t n)**: Computes the arccosine function of the given value
- **?t atan(?t n)**: Computes the arctangent function of the given value
- **?t exp(?t n)**: TODO
- **?t ln(?t n)**: TODO
- **?t log(?t n)**: TODO
- **?t pow(?t n)**: TODO
- **?t sqrt(?t n)**: TODO
- **?t cbrt(?t n)**: TODO
- **?t abs(?t n)**: TODO
- **?t min(?t n)**: TODO
- **?t max(?t n)**: TODO
- **?t floor(?t n)**: TODO
- **?t ceil(?t n)**: TODO



### Templates

Templates allow creating functions which uses the data types given by the function call.

A template can be declared like this:

```
FUNC ?a foo(?b bar)
	# Code
```

The above template takes two template arguments ``a`` and ``b``.



### Function calls

A function can be called using the following sentence:

```
foo(0)
```

The above instruction calls the ``foo`` function with the parameter ``0``.

If a function takes several parameters, they have to be seperated by a ``,``.



Calling a template function is almost the same as calling a normal function. Template parameters are determined according to the parameters passed to it.

However, if the return value is templated, then the type of the return value have to be given at function call (unless it's the same as one or several parameters).

Example:

```
foo<i8>(0)
```

The above instruction calls the ``foo`` template with parameter ``0`` and return type ``i8``.



## Documents selection

Documents are selected by the following parameters:
- **Path**: Which defines the directory or document where the request is going to work. If the path ends with ``/*``, then all the documents of the directory are going to be affected. If the path ends with ``/**``, then the selection will be **recursive**.
- **Condition**: Conditions allow to select files according to their content.



### Conditions

Conditions allow to select files according to the value of json fields contained in it.

The value of a field can be compared like this:

```
foo = "bar"
```

The previous condition checks if the ``foo`` field is a string and equals to ``"bar"``.

To use a field which is inside of an object, the used name should be ``object.field``. For example:

```json
{
	"foo":{
		"bar":""
	}
}
```

To use the ``bar`` field, ``foo.bar`` must be used.

Special variables are the following:
- **$name**: The document's name
- **$size**: The document's size

``%`` can be used in strings to specify that any characters can be placed at this location (but can be cancelled with ``\``).
